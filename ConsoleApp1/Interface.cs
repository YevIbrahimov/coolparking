﻿using System;
using System.Text.RegularExpressions;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;
using System.IO;
using System.Reflection;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;
using CoolParking.WebAPI.DTO;
using System.Text;

namespace UserInterface
{
	class Interface
	{
		static void Main(string[] args)
		{
			Hello();
		}

		private static void Hello()
		{
			Console.WriteLine("\t\tHello! Welcome to Cool Parking APP!\n\tYou can get info using commands from menu.\n\tWARNING! Do not use other numbers.\n\tHere You can get info about:");
			Menu();
		}
		private static void Menu()
		{
			ITimerService timer1 = new TimerService() { Interval = Settings.ChangeOffPeriod };
			ITimerService timer2 = new TimerService() { Interval = Settings.LogWritePeriod };
			ILogService log = new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log");

			IParkingService parking = new ParkingService(timer1, timer2, log);
			do
			{
				Console.WriteLine("\t\tMenu");
				Console.WriteLine("\t - Parking balance - 1\n\t " +
					"- Capacity - 2\n\t " +
					"- Free places - 3\n\t " +
					"- Vehicles at the parking - 4\n\t " +
					"- Vehicle by id - 5\n\t " +
					"- Put vehicle - 6\n\t " +
					"- Get vehicle from the parking  - 7\n\t " +
					"- Transactions for a period - 8\n\t " +
					"- All transactions - 9\n\t " +
					"- Top up vehicle - 10\n\t " +
					" Or write exit to out");
				var input = Console.ReadLine();
				Regex reg = new Regex(@"^[1-10]{1}$");
				if (!reg.IsMatch(input))
				{
					Console.WriteLine("No such option");
				}
				else
				{
					if (input.ToUpper() == "EXIT")
					{
						break;
					}
					HttpClient client = new HttpClient();
					switch (Int32.Parse(input))
					{
						case 1:
							var balance = client.GetAsync("http://localhost:44376/api/parking/balance");
							Console.WriteLine($"Balance is {balance}");
							break;
						case 2:
							var capacity = client.GetAsync("http://localhost:44376/api/parking/capacity");
							Console.WriteLine($"Capasity is : {capacity}");
							break;
						case 3:
							var freePlaces = client.GetAsync("http://localhost:44376/api/parking/freePlaces");
							Console.WriteLine($"Free places: {freePlaces}");
							break;
						case 4:
							var vehicles = JsonConvert.DeserializeObject<List<Vehicle>>(client.GetAsync("http://localhost:44376/api/vehicles").Result.Content.ReadAsStringAsync().Result);
							Console.WriteLine("Vehicles at the parking: ");
							foreach (var oneVehicle in vehicles)
							{
								Console.WriteLine($"{oneVehicle.Id} - {oneVehicle.VehicleType} - {oneVehicle.Balance}");
							}
							break;
						case 5:
							var vehicle = JsonConvert.DeserializeObject<Vehicle>(client.GetAsync("http://localhost:44376/api/vehicles/{id}").Result.Content.ReadAsStringAsync().Result);

							Console.WriteLine($"Vehicle: ID - {vehicle.Id}, Type - {vehicle.VehicleType}, Balance - {vehicle.Balance}");
							break;
						case 6:
							var newVehicle = JsonConvert.SerializeObject(new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), ChooseVehicle(), PutBalance()));
							var content = new StringContent(newVehicle, Encoding.UTF8, "application/json");
							client.PostAsync("http://localhost:44376/api/vehicles/", content);
							break;
						case 7:
							Console.WriteLine($"Enter vehicle id: ");
							string vehicleId = Console.ReadLine();

							client.DeleteAsync($"http://localhost:44376/api/vehicles/{vehicleId}");

							Console.WriteLine("Vehicle successfully get");
							break;
						case 8:
							var transactionsPeriod = JsonConvert.DeserializeObject<List<TransactionInfo>>(client.GetAsync("http://localhost:44376/api/transactions/last").Result.Content.ReadAsStringAsync().Result);
							Console.WriteLine("Transactions for a period: ");
							foreach (var transaction in transactionsPeriod)
							{
								Console.WriteLine($"{transaction.VehicleId}, {transaction.Sum}, {transaction.TransTime}");
							}
							break;
						case 9:
							var transactions = JsonConvert.DeserializeObject<List<TransactionInfo>>(client.GetAsync("http://localhost:44376/api/transactions/all").Result.Content.ReadAsStringAsync().Result);
							Console.WriteLine("All transactions: ");
							foreach (var transaction in transactions)
							{
								Console.WriteLine($"{transaction.VehicleId}, {transaction.Sum}, {transaction.TransTime}");
							}
							break;
						case 10:
							Console.WriteLine("Enter id: ");
							string id = Console.ReadLine();
							Console.WriteLine("Enter sum: ");
							decimal sum = decimal.Parse(Console.ReadLine());
							var tupUpParams = JsonConvert.SerializeObject(new TopUpVehicleDTO() { Id = id, Sum = sum });
							var tupUpContent = new StringContent(tupUpParams, Encoding.UTF8, "application/json");
							client.PutAsync("http://localhost:44376/api/transactions/all", tupUpContent);
							Console.WriteLine("Successfull operation");
							break;
					}
				}
			} while (true);
		}

		private static decimal PutBalance()
		{
			Console.WriteLine("Write your balance: ");
			decimal balance = Decimal.Parse(Console.ReadLine());
			while (balance < 0 || balance > Decimal.MaxValue) 
			{
				Console.WriteLine("Fake balance. Try again: ");
				balance = Decimal.Parse(Console.ReadLine());
			}
			return balance;
		}

		private static VehicleType ChooseVehicle()
		{
			Console.WriteLine("Choose vehicle: Passenger Car, Truck, Bus, Motorcycle: ");
			string vehicle = Console.ReadLine();
			while (vehicle.ToUpper() != "BUS" && vehicle.ToUpper() != "TRUCK" && vehicle.ToUpper() != "MOTORCYCLE" && vehicle.ToUpper() != "PASSENGER CAR")
			{
				Console.WriteLine("No such car, try again: ");
				vehicle = Console.ReadLine();
			}
			switch (vehicle.ToUpper())
			{
				case "PASSENGER CAR":
					return VehicleType.PassengerCar;
				case "TRUCK":
					return VehicleType.Truck;
				case "BUS":
					return VehicleType.Bus;
				case "MOTORCYCLE":
					return VehicleType.Motorcycle;
			}
			return 0;
		}
	}
}
