﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.DTO
{
	public class VehicleDTO
	{
		[JsonProperty("vehicleId")]
		[Required]
		public string Id { get; }
		[JsonProperty("vehicleType")]
		[Required]
		public VehicleType VehicleType { get; }
		[JsonProperty("balance")]
		[Required]
		public decimal Balance { get; internal set; }
	}
}
